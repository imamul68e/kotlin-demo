//https://youtu.be/dV5P8JVfiIc?list=PLsyeobzWxl7rooJFZhc3qPLwVROovGCfh
data class Data(val brand: String, val price: Int) {
    fun show() = println("Awesome machine")
}

fun main(args: Array<String>) {
    var d1 = Data("Dell", 2100)
    var d2 = Data("Acer", 2000)

    println(d1)
    println(d1.equals(d2))
    println(d1 == d2)

    var d3 = d1.copy()
    println(d3)

    var d4 = d1.copy(price = 3000)
    println(d4)
}