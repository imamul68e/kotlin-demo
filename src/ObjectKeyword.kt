//https://youtu.be/kH4pSAFSheU?list=PLsyeobzWxl7rooJFZhc3qPLwVROovGCfh
data class Book(var name: String, var price: Int)

object ObjectKeyword {
    var books = arrayListOf<Book>()
    fun showBooks() {
        for (book in books) {
            println(book)
        }
    }
}

fun main(args: Array<String>) {
    ObjectKeyword.books.add(Book("JAVA", 20))
    ObjectKeyword.books.add(Book("SQL", 25))
    ObjectKeyword.books.add(Book("KOTLIN", 23))

    ObjectKeyword.showBooks();
}