//https://youtu.be/kvpvem7BLq0?list=PLsyeobzWxl7rooJFZhc3qPLwVROovGCfh
abstract class AbstractClass {
    abstract fun callMe()
    fun calling() = println("Calling...")
}

class SubClass2 : AbstractClass() {
    override fun callMe() = println("From Sub class");
}


fun main(args: Array<String>) {
    var inh = SubClass2();
    inh.callMe();
    inh.calling()
}
