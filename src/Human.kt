//https://youtu.be/6RIJrnZI7jk?list=PLsyeobzWxl7rooJFZhc3qPLwVROovGCfh
//primary constructor
class Human(n: String = "") {
    //if n is not initialized with value (may have default value) then value is to be passed during calling
    var name: String = n
    fun think() = println("This is human...${name}")
}

class Human2(n: String) {
    var name: String = n

    //can be used the following lines
//    var name: String = ""
//
//    init {
//        name = n
//    }

    fun think() = println("This is human2...${name}")
}

//https://youtu.be/HZLr7XDW7ak?list=PLsyeobzWxl7rooJFZhc3qPLwVROovGCfh
//secondary constructor
class Human3(n: String) {
    var name: String = n
    var age: Int = 0

    constructor(name: String, age: Int) : this(name) {
        this.age = age
    }

    fun think() = println("This is human...${name}: ${age}")
}

fun main(args: Array<String>) {
    //primary constructor
    var human = Human("Imamul")
    human.think()

    var human2 = Human2("Imamul") //must send value here
    human2.think()

    //secondary constructor
    var human3 = Human3("Imamul", 20)
    human3.think()
}
