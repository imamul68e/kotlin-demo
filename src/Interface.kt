//https://youtu.be/jpp6yAEPB0Y?list=PLsyeobzWxl7rooJFZhc3qPLwVROovGCfh
interface Interface {
    fun show();

    fun print() = println("From Interface")
}

interface Interface2 {
    fun display();
    fun print() = println("From Interface2")
}

class Obj : Interface, Interface2 {
    override fun show() = println("Show")
    override fun display() = println("Display")
    override fun print() = println("From Obj")

    fun printSpecific() = super<Interface2>.print()
}

fun main(args: Array<String>) {
    var obj = Obj();
    obj.show()
    obj.display()
    obj.print()
    obj.printSpecific()
}