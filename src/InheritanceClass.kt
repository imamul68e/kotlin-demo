//https://youtu.be/XR3XueD8_yM?list=PLsyeobzWxl7rooJFZhc3qPLwVROovGCfh
open class InheritanceClass(age: Int) {
    init {
        println("From Inheritance class->constructor, age is ${age}");
    }

    open fun callMe() = println("From Inheritance class");
}

class SubClass(age: Int) : InheritanceClass(age) { //here age can be of different value
    init {
        println("From Sub class->constructor, age is ${age}");
    }

    override fun callMe() = println("From Sub class");
}


fun main(args: Array<String>) {
    var inh = SubClass(20);
    inh.callMe();
}
