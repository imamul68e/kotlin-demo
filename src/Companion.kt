//https://youtu.be/Xiqfy0OpZnc?list=PLsyeobzWxl7rooJFZhc3qPLwVROovGCfh
class A {
    companion object {
        @JvmStatic
        fun show() = println("Show me");

        fun create(): A = A()
    }

    fun show2() = println("Show me 2")

}

fun main(args: Array<String>) {
    A.show()

    //companion object in factory pattern
    var obj = A.create()
    obj.show2()
}