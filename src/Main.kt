import java.math.BigInteger
import java.util.Date

fun main() {
    println("Hello World!")

    var test = Test();

    test.name = "Imamul";

    println("My name is " + test.name);

    println("My name: ${test.name}");


    var javaTest = TestJava();
    //no need getter and setter
    javaTest.name = "Hossain"
    println("My name: ${javaTest.name}");

    val num: Int = 2

//    switch case
    var numString = when (num) {
        1 -> "This is one"
        2 -> "This is two"
        else -> "Invalid"
    }

    println(numString)

    //num array and loop
    var nums = 1..10

    for (a in nums) {
        println(a)
    }

    //print how many steps
    for (a in nums step 2) {
        println(a)
    }

    //desc nums
    var numss = 10 downTo 1; //similar to 10.downTo(1)

    for (a in numss) {
        println(a)
    }

    //until: print up to 9
    var numsss = 1 until 10

    for (a in numsss) {
        println(a)
    }

    //list values
    var list = listOf(1, 2, 5, 6)
    for (a in list) {
        println(a)
    }
    // print list value with index
    for (a in list.withIndex()) {
        println("${a.index} ${a.value}")
    }

    // can be written
    for ((i, v) in list.withIndex()) {
        println("$i $v")
    }

    //map
    var map = HashMap<String, Int>();

    map.put("a", 1);
    map.put("b", 2);
    map["c"] = 3;

    for ((k, v) in map) {
        println("$k $v")
    }

    //function
    println(add(1, 2))
    //inline function
    println(sub(1, 2))

    //max value
    println(max(1, 2))

    //extension function
    var t1 = Test();
    t1.name = "TEST1"
    t1.show()

    var t2 = Test();
    t2.name = "TEST2"
    t2.show()

    var t3 = t1.plus(t2)
    t3.show()

    var t4 = t1 plus t2 //Infix function
    t4.show()

    var t5 = t1 + t2 //operator overloading
    t5.show()

    //recursion: max no is 31, use BigInt and tail recursion instead
    println(fact(31))
    println(factBig(BigInteger("6200"), BigInteger.ONE))
}

fun max(a: Int, b: Int): Int = if (a > b) a else b

//inline function
fun sub(a: Int, b: Int): Int = a - b

fun add(a: Int, b: Int): Int {
    return a + b
}

//operator overloading with "+" sign and infix
operator infix fun Test.plus(t: Test): Test {
    var tNew = Test();
    tNew.name = this.name + ", " + t.name
    return tNew
}

fun fact(num: Int): Int = if (num == 0) 1 else num * fact(num - 1)

//https://youtu.be/5rFOLyxjmOg?list=PLsyeobzWxl7rooJFZhc3qPLwVROovGCfh
tailrec fun factBig(num: BigInteger, result: BigInteger): BigInteger =
    if (num == BigInteger.ZERO) result else factBig(num - BigInteger.ONE, num * result)