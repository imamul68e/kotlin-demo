interface AnonymousClass {
    fun call()
}

fun main(args: Array<String>) {
    var cls: AnonymousClass = object : AnonymousClass {
        override fun call() = println("call me")
    }

    cls.call()
}